package com.CleanCoding.Clean.Coding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CleanCodingApplication {

    public static void main(String[] args) {
        SpringApplication.run(CleanCodingApplication.class, args);
    }

}

package com.CleanCoding.Clean.Coding.entity;


import javax.persistence.*;
import java.util.List;

// Used class name as Orders instead of Order because it is reserved keyword in MySQL.
@Entity
@Table(name = "Orders")
public class Orders {
    @Id
    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "productId")
    @OneToMany
    private List<Product> productList;

    public Orders() {
    }

    public Orders(Long orderId, List<Product> productList) {
        this.orderId = orderId;
        this.productList = productList;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}

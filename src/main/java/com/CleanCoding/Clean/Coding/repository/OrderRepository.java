package com.CleanCoding.Clean.Coding.repository;

import com.CleanCoding.Clean.Coding.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Orders, Long> {
}

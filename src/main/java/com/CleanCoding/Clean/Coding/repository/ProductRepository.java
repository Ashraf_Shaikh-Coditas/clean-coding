package com.CleanCoding.Clean.Coding.repository;

import com.CleanCoding.Clean.Coding.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

}

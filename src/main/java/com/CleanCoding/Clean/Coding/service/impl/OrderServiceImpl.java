package com.CleanCoding.Clean.Coding.service.impl;

import com.CleanCoding.Clean.Coding.entity.Orders;
import com.CleanCoding.Clean.Coding.repository.OrderRepository;
import com.CleanCoding.Clean.Coding.service.impl.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public Orders placeOrder(Orders order) {
        Orders savedOrder = orderRepository.save(order);
        return savedOrder;
    }

    @Override
    public String cancelOrder(Long orderId) {
        Optional<Orders> order = orderRepository.findById(orderId);
        if (order.isPresent()) {
            orderRepository.delete(order.get());
        }
        return "Order with Id : " + orderId + " cancelled Successfully";
    }

    @Override
    public List<Orders> getAllOrders() {
        List<Orders> ordersList = orderRepository.findAll();
        return ordersList;
    }
}

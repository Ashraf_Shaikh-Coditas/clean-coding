package com.CleanCoding.Clean.Coding.service.impl;

import com.CleanCoding.Clean.Coding.entity.Product;
import com.CleanCoding.Clean.Coding.repository.ProductRepository;
import com.CleanCoding.Clean.Coding.service.impl.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product addProduct(Product product) {
        Product savedProduct = productRepository.save(product);
        return savedProduct;
    }

    @Override
    public String deleteProduct(Long productId) {
        Optional<Product> product = productRepository.findById(productId);
        if (product.isPresent()) {
            productRepository.delete(product.get());
        }
        return "Product with Id : " + productId + " deleted Successfully";
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> productList = productRepository.findAll();
        return productList;
    }
}

package com.CleanCoding.Clean.Coding.service.impl.service;

import com.CleanCoding.Clean.Coding.entity.Orders;

import java.util.List;

public interface OrderService {

    Orders placeOrder(Orders order);

    String cancelOrder(Long orderId);

    List<Orders> getAllOrders();
}

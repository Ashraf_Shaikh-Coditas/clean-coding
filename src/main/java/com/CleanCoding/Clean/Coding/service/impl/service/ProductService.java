package com.CleanCoding.Clean.Coding.service.impl.service;

import com.CleanCoding.Clean.Coding.entity.Product;

import java.util.List;

public interface ProductService {
    Product addProduct(Product product);

    String deleteProduct(Long productId);

    List<Product> getAllProducts();
}
